#include "integer_avg.h"
#include "unity.h"

#include <stdlib.h>
#include <string.h>

void setUp(void)
{
    /* Set stuff up here */
}

void tearDown(void)
{
    /* Clean stuff up here */
}

void test_avg_function(void)
{
    /* Check result */
    TEST_ASSERT_TRUE_MESSAGE(average(-2,-2)<0, "Error in sign");
    TEST_ASSERT_EQUAL_INT_MESSAGE(0,average(1,0), "Error with param 0");
    
}
